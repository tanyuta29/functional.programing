import java.util.ArrayList;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Second1 {
public static void DemoStream(){
    ArrayList <Integer> AL= new ArrayList<>();
    int number;
    Random rnd= new Random();
    for (int i=0; i<10;i++){
        number = rnd.nextInt()%100;
        AL.add(number);

    }
    System.out.println("Array:" + AL);
    Stream<Integer> st=AL.stream();
    Predicate<Integer> fn;
    fn= (n)->(n%2)==0;
    Stream<Integer> resStream = st.filter(fn);
    System.out.println("n =" + resStream.count());
    int n2 = (int) (AL.stream().filter((n)->(n%2)==0)).count();
    System.out.println("n2 =" + n2);

}

    public static void main(String[] args) {
        DemoStream();
    }
}


