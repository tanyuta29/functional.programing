import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class First {
    static List<Integer> numbers = new ArrayList<>();
    public static List getNumbers(){
        numbers.add(10);
        numbers.add(56);
        numbers.add(66);
        numbers.add(890);
        numbers.add(12);
        numbers.add(28);
        numbers.add(29);
        numbers.add(46);
        numbers.add(75);
        numbers.add(78);
        return numbers;
    }

    public static void main(String[] args) {
        List<Integer> myNumber = getNumbers();
        System.out.println("Сума квадратів:");
        myNumber.stream()
                .map(numbers->numbers * 2)
                .reduce(0, (a,b)-> a + b, Integer::sum)
                ;
}
}
